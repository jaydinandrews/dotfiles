if has('vim')
	set nocompatible
endif

call plug#begin('~/.vim/plugged')

" styling
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" vimwiki
Plug 'vimwiki/vimwiki'

" nerdtree
Plug 'preservim/nerdtree'

call plug#end()

filetype plugin indent on
syntax enable
colorscheme slate
" let mapleader=','

" encoding
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8
scriptencoding utf-8

" show invisible chars
set list listchars=tab:▸…,eol:¬,nbsp:%

" indentation
set tabstop=2
set softtabstop=0
set shiftwidth=2
set expandtab

set number relativenumber

"styling
let g:airline_theme = 'angr'
let g:airline_powerline_fonts = 1
let g:powerline_pycmd = 'py3'

" vimwiki working dir
let g:vimwiki_list = [{'path': '~/.vimwiki/'}]

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif
let g:airline_symbols.linenr = ''
